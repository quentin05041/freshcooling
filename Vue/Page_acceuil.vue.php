<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <link rel="stylesheet" href="../Vue/Page_accueil.vue.css">
    <meta charset="utf-8">
    <title>Bienvenue sur FreshCooling</title>
  </head>
  <body>
    <nav>
      <a href="page_acceuil.controler.php?id=1"><p>Accueil</p><img src="../model/Data/photo/home.png" alt="logo"></a>
      <a href="controler_page_principale.php?categorie=WaterBlocks"><p>Water Blocks</p><img src="../model/Data/photo/Blocks.png" alt="logo"></a>
      <a href="controler_page_principale.php?categorie=Reservoirs"><p>Reservoirs</p><img src="../model/Data/photo/Reservoirs.png" alt="logo"></a>
      <a href="controler_page_principale.php?categorie=Pompes"><p>Pompes</p><img src="../model/Data/photo/Pompes.png" alt="logo"></a>
      <a href="controler_page_principale.php?categorie=Dissipateurs"><p>Dissipateurs</p><img src="../model/Data/photo/Dissipateurs.png" alt="logo"></a>
      <a href="controler_page_principale.php?categorie=Fixations"><p>Fixations</p><img src="../model/Data/photo/Fixations.png" alt="logo"></a>
      <?php

       if(isset($_SESSION['mail'])) {
          echo "<a href=\"#\"><p>", $_SESSION['mail']," est connecté </p></a>";
          echo"<a href=\"../controler/deconnexion.controler.php\"><p>Déconnexion</p></a>";

      }  else {
        echo "<a href=\"../Vue/connexion.vue.php\"><p><description>Connecter vous</description></p></a>";
        echo"<a href=\"../Vue/Inscription.vue.php\"><p>Créer un compte</p></a>";
      }?>
    </nav>
    <h1>Nos produits les plus performants</h1>
<div class="container">
    <?php

    echo "<a href=\"page_acceuil.controler.php?id=$idleft\"><img src=\"../model/Data/photo/left.png\" alt=\"left\"></a>";
    echo "<img class=\"$articleActu->cover\" src=\"../model/Data/photo/$articleActu->cover\" alt=\"performant\">";
    echo "<a href=\"page_acceuil.controler.php?id=$idright\"><img src=\"../model/Data/photo/right.png\" alt=\"right\"></a>";
    ?>

</div>
  </body>
</html>
