<!DOCTYPE html>
<html lang=fr dir="ltr">
  <head>
    <link rel="stylesheet" href="Inscription.vue.css">
    <meta charset="utf-8">
    <title>Inscription</title>
  </head>
  <body>
    <nav>
      <a href="../controler/page_acceuil.controler.php?id=1"><p>Accueil</p><img src="../model/Data/photo/home.png" alt="../model/Data/photo/logo"></a>
      <a href="../controler/controler_page_principale.php?categorie=WaterBlocks"><p>Water Blocks</p><img src="../model/Data/photo/Blocks.png" alt="logo"></a>
      <a href="../controler/controler_page_principale.php?categorie=Reservoirs"><p>Reservoirs</p><img src="../model/Data/photo/Reservoirs.png" alt="logo"></a>
      <a href="../controler/controler_page_principale.php?categorie=Pompes"><p>Pompes</p><img src="../model/Data/photo/Pompes.png" alt="logo"></a>
      <a href="../controler/controler_page_principale.php?categorie=Dissipateurs"><p>Dissipateurs</p><img src="../model/Data/photo/Dissipateurs.png" alt="logo"></a>
      <a href="../controler/controler_page_principale.php?categorie=Fixations"><p>Fixations</p><img src="../model/Data/photo/Fixations.png" alt="logo"></a>
    </nav>
    <form class="" action="../controler/inscription.controleur.php?id=1" method="post">


<div class="item">
      <p>Pseudo :</p>
      <input type="text" name="pseudo"placeholder="Pseudo" required>
</div>
<div class="item">
      <p>E-mail :</p>
      <input type="text" name="e-mail" placeholder="E-mail" required>
</div>
<div class="item">
      <p>Mot de passe :</p>
      <input type="password" name="mdp" placeholder="Mot de passe" required>
</div>
<div class="item">
      <p>Confirmation mot de passe :</p>
      <input type="password" name="mdpConfirm" placeholder="Confirmation" required>
</div>
      <input class="connexion" type="submit" name="connexion" value="Inscription">
    </form>

  </body>
</html>
