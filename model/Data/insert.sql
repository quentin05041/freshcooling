INSERT into article(article_id,title,prix,description,cover,categorie) values (1,'Ek-Annihilator',141.07,'EXP/EP Square','waterblock_1.jpg','WaterBlocks');
INSERT into article(article_id,title,prix,description,cover,categorie) values (2,'Ek-FC1080 GTX Ti',151.21,'Aorus RGB - Nickel','waterblock_2.jpg','WaterBlocks');
INSERT into article(article_id,title,prix,description,cover,categorie) values (3,'EK-Velocity-AMD',70.49,'Copper + Acetal','waterblock_3.jpg','WaterBlocks');
INSERT into article(article_id,title,prix,description,cover,categorie) values (4,'EK-Velocity-RGB',90.66,'Nickel + Acetal','waterblock_4.jpg','WaterBlocks');
INSERT into article(article_id,title,prix,description,cover,categorie) values (5,'Ek-Vector RTX',44.27,'Backplate-Nickel','waterblock_5.jpg','WaterBlocks');

INSERT into article(article_id,title,prix,description,cover,categorie) values (6,'Ek-XRES 140 Revo',90.71,'D5 RGB(stand-alone)','reservoir_1.jpg','Reservoirs');
INSERT into article(article_id,title,prix,description,cover,categorie) values (7,'Ek-XRES 140 Revo',95.75,'D5 - Plexi(stand-alone)','reservoir_2.jpg','Reservoirs');
INSERT into article(article_id,title,prix,description,cover,categorie) values (8,'Ek-RES X3 250',70.55,'RGB','reservoir_3.jpg','Reservoirs');
INSERT into article(article_id,title,prix,description,cover,categorie) values (9,'Ek-RES X4 250',131.05,'(R2.0)','reservoir_4.jpg','Reservoirs');
INSERT into article(article_id,title,prix,description,cover,categorie) values (10,'Ek-RES X3 150',47.35,'Lite','reservoir_5.jpg','Reservoirs');


INSERT into article(article_id,title,prix,description,cover,categorie) values (11,"Ek-XTOP Revo D5",55.42,"RGB - Plexi(stand-alone)","pompe_1.png","Pompes");
INSERT into article(article_id,title,prix,description,cover,categorie) values (12,'Ek-XTOP Revo D5',141.13,'RGB PWM - Plexi(incl. sl. pump)','pompe_2.png','Pompes');
INSERT into article(article_id,title,prix,description,cover,categorie) values (13,'Ek-XTOP SPC-60',68.53,'PWM - Plexi(incl. pump)','pompe_3.png','Pompes');
INSERT into article(article_id,title,prix,description,cover,categorie) values (14,'Ek-XTOP SPC-60',68.53,'RGB - Acetal(incl. pump)','pompe_4.png','Pompes');
INSERT into article(article_id,title,prix,description,cover,categorie) values (15,'Ek-D5 PWM G2',75.59,'Motor(12V DC PWM Pump Motor)','pompe_5.png','Pompes');


INSERT into article(article_id,title,prix,description,cover,categorie) values (16,"EK-Vardar EVO 140ER",22.14,"White BB (500-2000rpm)","dissipateur_1.png","Dissipateurs");
INSERT into article(article_id,title,prix,description,cover,categorie) values (17,'EK-VARDAR EVO 140S',20.12,'BB(500-1150rpm)','dissipateur_2.png','Dissipateurs');
INSERT into article(article_id,title,prix,description,cover,categorie) values (18,'EK-Furious Vardar',18.11,'EVO 120 BB(750-3000rpm)','dissipateur_3.png','Dissipateurs');
INSERT into article(article_id,title,prix,description,cover,categorie) values (19,'EK-Vardar EVO 120ER',22.08,'RGB (500-2200rpm)','dissipateur_4.png','Dissipateurs');
INSERT into article(article_id,title,prix,description,cover,categorie) values (20,'EK-CoolStream SE',110.88,'560','dissipateur_5.png','Dissipateurs');

INSERT into article(article_id,title,prix,description,cover,categorie) values (21,"EK-PLUG-G/4",9.83,"Acetal-Black(10 pack)","fixation_1.png","Fixations");
INSERT into article(article_id,title,prix,description,cover,categorie) values (22,'Ek-HDC Fitting',47.35,'16mm - Red(6-pack)','fixation_2.png','Fixations');
INSERT into article(article_id,title,prix,description,cover,categorie) values (23,'EK-ACF Fitting',34.24,'10/16mm-Nickel(6-pack)','fixation_3.png','Fixations');
INSERT into article(article_id,title,prix,description,cover,categorie) values (24,'EK-ACF Fitting',34.24,'10/13mm - Black (6-pack)','fixation_4.png','Fixations');
INSERT into article(article_id,title,prix,description,cover,categorie) values (25,'EK-HDC Fitting',23.15,'12mm - Black(4-pack)','fixation_5.png','Fixations');
