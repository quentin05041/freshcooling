CREATE TABLE if not exists membres(
  pseudo TEXT UNIQUE,
  mail TEXT UNIQUE PRIMARY KEY,
  mdp TEXT
);



CREATE TABLE if not exists article(
  article_id SMALLINT PRIMARY KEY,
  title VARCHAR[30],
  prix NUMERIC DEFAULT 0,
  description TEXT DEFAULT "Pas de description",
  cover VARCHAR[30],
  categorie VARCHAR[30]
);

CREATE TABLE if not exists panier(
  panier_id SMALLINT PRIMARY KEY,
  user_id SMALLINT,
  article_id SMALLINT,
  FOREIGN KEY (article_id) REFERENCES article(article_id),
  FOREIGN KEY (user_id) REFERENCES membre(membre_id)
);
