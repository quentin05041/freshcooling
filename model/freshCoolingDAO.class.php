<?php
  class freshCoolingDAO{
    private $db;

    function __construct(){
      try{
        $this->db = new PDO('sqlite:../model/Data/freshCooling.db');
      }
      catch(PDOException $e){
        die("erreur de connexion :".$e->getmessage());
      }
    }


    function getArticle($categorie): array{
      $sql = "SELECT * FROM article WHERE categorie='$categorie'";
      $sth = $this->db->query($sql);
      $res = $sth->fetchAll(PDO::FETCH_CLASS,'Article');
      if(count($res) == 0){
        throw new \Exception("Id pas trouvé");
      }
      else {
        return $res;
      }
    }

    function getArticleID($ID): Article{
      $sql = "SELECT * FROM article WHERE article_id='$ID'";
      $sth = $this->db->query($sql);
      $res = $sth->fetchAll(PDO::FETCH_CLASS,'Article');
      if(count($res) == 0){
        throw new \Exception("Id pas trouvé");
      }
      else {
        return $res[0];
      }
    }

    function insertMembre($pseudo,$mdp,$mail) {
      $query=$this->db->prepare('INSERT INTO membres (pseudo, mdp, mail) VALUES (:pseudo, :pass, :mail)');

      $query->bindValue(':pseudo', $pseudo, PDO::PARAM_STR);
      $query->bindValue(':pass', $mdp, PDO::PARAM_STR);
      $query->bindValue(':mail', $mail, PDO::PARAM_STR);
      $query->execute();
      $query->CloseCursor();
    }

    function getMailMdp($mail) : array {
      $sql = "SELECT mail,mdp FROM membres WHERE mail='$mail'";
      $sth = $this->db->query($sql);
      $res = $sth->fetchAll(PDO::FETCH_CLASS,'Membres');


      return $res;
    }


    function getInfoMembre($mail) : bool{
     $sql = "SELECT COUNT(*) AS nbr FROM membres WHERE mail='$mail'";
     $sth = $this->db->query($sql);
     $dispo = ($sth->fetchColumn()==0)?1:0;


     return $dispo;
    }
  }

 ?>
